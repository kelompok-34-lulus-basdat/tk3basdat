from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def form_create_produk_apotek(request):
    query = """
    SELECT id_produk
    FROM PRODUK;
    """

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_id_produk = fetch(cursor)

    query = """
    SELECT id_apotek
    FROM APOTEK;
    """

    cursor.execute(query)   
    data_id_apotek = fetch(cursor) 

    context = {
        'data_id_produk':data_id_produk,
        'data_id_apotek':data_id_apotek, 
    }
    return render(request, 'create-produkapotek.html', context)

def create_pa(request):
    pa = {
        "id_produk": request.POST.get('id_produk'),
        "id_apotek": request.POST.get('id_apotek'),
        "harga_jual": request.POST.get('harga_jual'),
        "satuan_penjualan": request.POST.get('satuan_penjualan'),
        "stok": request.POST.get('stok')
    }

    query = """
    INSERT INTO PRODUK_APOTEK (harga_jual, stok, satuan_penjualan, id_produk, id_apotek)
    VALUES ('%s', '%s', '%s', '%s', '%s');
    """ %(pa['harga_jual'], pa['stok'], pa['satuan_penjualan'], pa['id_produk'], pa['id_apotek'])

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('produkapotek:mylistpa')

def update_pa(request, idpro, idapo):
    pa = {
        "id_produk": request.POST.get('pa-id-produk'),
        "id_apotek": request.POST.get('pa-id-apotek'),
        "harga_jual": request.POST.get('pa-harga-jual'),
        "satuan_penjualan": request.POST.get('pa-satuan-penj'),
        "stok": request.POST.get('pa-stok')
    }
    qidlama = """
    SELECT id_produk, id_apotek
    FROM PRODUK_APOTEK
    WHERE id_produk = '%s' AND id_apotek = '%s';
    """%(idpro, idapo)

    cursor.execute(qidlama)
    data_pa_id_lama = fetch(cursor)

    for idlama in data_pa_id_lama:
        query = """
        DELETE FROM PRODUK_APOTEK
        WHERE id_produk = '%s' AND id_apotek = '%s';
        """%(idlama['id_produk'], idlama['id_apotek'])
        cursor.execute(query)

    query = """
    INSERT INTO PRODUK_APOTEK (harga_jual, stok, satuan_penjualan, id_produk, id_apotek)
    VALUES ('%s', '%s', '%s', '%s', '%s');
    """ %(pa['harga_jual'], pa['stok'], pa['satuan_penjualan'], pa['id_produk'], pa['id_apotek'], idpro, idapo)

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('produkapotek:mylistpa')

def delete_pa(request, idpro, idapo):
    query = """
    DELETE FROM PRODUK_APOTEK
    WHERE id_produk = '%s' AND id_apotek = '%s';
    """%(idpro, idapo)

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('produkapotek:mylistpa')

def list_pa(request):
    query = """
    SELECT * FROM PRODUK_APOTEK
    ORDER BY CAST(id_produk AS varchar);
    """
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_produk_apotek = fetch(cursor)
    return render(request, 'list-produkapotek.html', {'data_produk_apotek':data_produk_apotek})

#converts sql rows to a dict
def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results