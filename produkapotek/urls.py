from django.urls import path, include
from . import views

app_name = "produkapotek"

urlpatterns = [
    path('createpa/', views.create_pa, name='mycreatepa'),
    path('updatepa/<idpro>/<idapo>', views.update_pa, name='myupdatepa'),
    path('listpa/', views.list_pa, name='mylistpa'),
    path('formcreatepa/', views.form_create_produk_apotek, name='myformcreatepa'),
    path('deletepa/<idpro>/<idapo>', views.delete_pa, name='mydeletepa<idpro>/<idapo>'),
]
