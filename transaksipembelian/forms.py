from django import forms
from django.db import connection

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
def idKonsumen():
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to FARMAKAMI;"
                        "SELECT DISTINCT id_konsumen "
                        "FROM KONSUMEN;" )
        id_k = dictfetchall(cursor)
        daftar_id_k = []
        for id in id_k:
            temp = []
            temp.append(id['id_konsumen'])
            temp.append(id['id_konsumen'])
            daftar_id_k.append(tuple(temp))
        return daftar_id_k
pillihan_id_konsumen = idKonsumen()

# def id_konsumen_terntentu(email):
#      with connection.cursor() as cursor:
#         cursor.execute("SET search_path to FARMAKAMI;"
#                         "SELECT  id_konsumen "
#                         "FROM KONSUMEN "
#                         "WHERE email = '{}';".format(email) )
#         id_k = dictfetchall(cursor)
#         daftar_id_k = []
#         for id in id_k:
#             temp = []
#             temp.append(id['id_konsumen'])
#             temp.append(id['id_konsumen'])
#             daftar_id_k.append(tuple(temp))
#         return daftar_id_k
class createTransaksiPembelianForm(forms.Form):
    # roleNow = request.session.get("role")
    # if roleNow == "KONSUMEN":
    #     email = request.session.get("email")
    #     id_konsumen = forms.CharField(label = 'ID Konsumen', widget=forms.HiddenInput(), initial=)
    id_konsumen = forms.CharField(label = 'ID Konsumen',widget=forms.Select(choices=pillihan_id_konsumen, attrs={'class': 'form-control'}))
class updateTransaksiPembelianForm(forms.Form):
    id_konsumen =  forms.CharField(label = 'ID Konsumen',widget=forms.Select(choices=pillihan_id_konsumen, attrs={'class': 'form-control'}))