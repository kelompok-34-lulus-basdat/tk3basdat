from django.urls import path
from . import views

app_name = "transaksipembelian"

urlpatterns = [
    path('create-transaksipembelian/',views.create_transaksipembelian, name='create-transaksipembelian'),
    path('daftar-transaksipembelian/',views.daftar_transaksipembelian,name='daftar-transaksipembelian'),
    path('update-transaksipembelian/<id>',views.update_transaksipembelian,name='update-transaksipembelian'),
    path('delete-transaksipembelian/<id>',views.delete_transaksipembelian,name='deletransaksi<id>'),
    
]