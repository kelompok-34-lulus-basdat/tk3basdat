from django.shortcuts import render, redirect
from django.db import connection
from .forms import createTransaksiPembelianForm
import datetime
from django.core.paginator import Paginator
from .forms import updateTransaksiPembelianForm

# Create your views here.
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def create_transaksipembelian(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            id_konsumen = request.POST.get('id_konsumen')
            cursor.execute("SET search_path TO FARMAKAMI;")
            cursor.execute("SELECT COUNT(*) AS jumlah_transaksi FROM TRANSAKSI_PEMBELIAN;")
            id_transaksi = 'trd' +  str(int((dictfetchall(cursor)[0])['jumlah_transaksi']) + 1)
            waktu = datetime.datetime.now()
            total_pembayaran = 0

            #INSERT into TRANSAKSI PEMBELIAN
            cursor.execute("SET search_path to FARMAKAMI;")
            cursor.execute("INSERT INTO TRANSAKSI_PEMBELIAN(id_konsumen, id_transaksi_pembelian, waktu_pembelian, total_pembayaran) "
                            "VALUES ('{}', '{}', '{}', '{}');".format(id_konsumen,id_transaksi,waktu,total_pembayaran))
            return redirect("transaksipembelian:daftar-transaksipembelian")
    form = createTransaksiPembelianForm()
    args ={
        'form' : form
    }
    return render(request, 'create-transaksipembelian.html',args)
def daftar_transaksipembelian(request):
    roleNow = request.session.get("role")
    email = request.session.get("email")
    if roleNow == "KONSUMEN":
        query = """
        SELECT T.id_transaksi_pembelian, T.waktu_pembelian, T.total_pembayaran, T.id_konsumen
        FROM TRANSAKSI_PEMBELIAN T, KONSUMEN K
        WHERE T.id_konsumen = K.id_konsumen AND K.email = '%s';
        """%(email)
    else:
        query = """
        SELECT * 
        FROM TRANSAKSI_PEMBELIAN;
        """
    cursor = connection.cursor()
    cursor.execute("SET search_path to FARMAKAMI;")
    cursor.execute(query)

    data_transaksipembelian = fetch(cursor)
    form = updateTransaksiPembelianForm()
    args = {
        'form': form,
        'data_transaksipembelian': data_transaksipembelian
    }
    return render(request,'daftar-transaksipembelian.html', args)
def update_transaksipembelian(request,id):
    id_konsumen = request.POST.get('u-id-konsumen')
    query = """
    UPDATE TRANSAKSI_PEMBELIAN
    SET id_konsumen = '%s' WHERE id_transaksi_pembelian = '%s';
    """ %(id_konsumen,id)
    cursor = connection.cursor()
    cursor.execute("SET search_path to FARMAKAMI;")
    cursor.execute(query)

    return redirect('transaksipembelian:daftar-transaksipembelian')

def delete_transaksipembelian(request,id):
    query="""
    DELETE FROM TRANSAKSI_PEMBELIAN
    WHERE id_transaksi_pembelian = '%s';
    """ %(id)

    cursor = connection.cursor()
    cursor.execute("SET search_path to FARMAKAMI;")
    cursor.execute(query)

    return redirect('transaksipembelian:daftar-transaksipembelian')
def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]