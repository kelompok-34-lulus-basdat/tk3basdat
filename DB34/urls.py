"""DB34 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('apotek/', include('apotek.urls')),
    path('produkapotek/', include('produkapotek.urls')),
    path('users/', include('users.urls')),
    path('profile/',include('profilpengguna.urls')),
    path('balai-pengobatan/', include('balaipengobatan.urls')),
    path('obat/', include('obat.urls')),
    path('transaksipembelian/',include('transaksipembelian.urls')),
    path('pengantaranfarmasi/',include('pengantaranfarmasi.urls')),
    path('listprodukdibeli/',include('listprodukdibeli.urls')),
]
