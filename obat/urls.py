from django.urls import path
from . import views

app_name = "obat"

urlpatterns = [
    path('daftarobat/', views.daftar_obat_view, name='mydaftarobat'),
    path('formbuatobat/', views.form_buat_obat_view, name='myformbuatobat'),
    path('buatobat/', views.buat_obat, name='mybuatobat'),
    path('updateobat/<id>', views.update_obat, name='myupdateobat'),
    path('deleteobat/<id>', views.delete_obat, name='mydeleteobat<id>'),
]
