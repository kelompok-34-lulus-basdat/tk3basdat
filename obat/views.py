from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def daftar_obat_view(request):
    query = """
	SELECT *
	FROM OBAT
    ORDER BY CAST(id_obat AS integer);
	"""
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_obat = fetch(cursor)

    return render(request, 'daftarobat.html', {'data_obat':data_obat})

def form_buat_obat_view(request):
    query = """
	SELECT id_merk_obat
	FROM merk_obat;
	"""
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_id_merk_obat = fetch(cursor)
    return render(request, 'buatobat.html', {'data_id_merk_obat':data_id_merk_obat})

def buat_obat(request):
    obat = {
            "id-obat" : get_id_obat(),
            "id-produk" : get_id_produk(),
            "id-merk-obat" : request.POST.get('id-merk-obat'),
            "netto" : request.POST.get('netto'),
            "dosis" : request.POST.get('dosis'),
            "aturan-pakai" : request.POST.get('aturan-pakai'),
            "kontraindikasi" : request.POST.get('kontraindikasi'),
            "btk-kesediaan" : request.POST.get('btk-kesediaan')
    }

    query =  """
    INSERT INTO OBAT (id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontraindikasi, bentuk_kesediaan)
    VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s');
    """ %(obat['id-obat'], obat['id-produk'], obat['id-merk-obat'], obat['netto'], obat['dosis'], obat['aturan-pakai'], obat['kontraindikasi'], obat['btk-kesediaan'])
    
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)
    
    return redirect('obat:mydaftarobat')

def update_obat(request, id):
    obat = {
            "id-obat" : request.POST.get('u-id-obat'),
            "id-produk" : request.POST.get('u-id-produk'),
            "id-merk-obat" : request.POST.get('u-id-merk-obat'),
            "netto" : request.POST.get('u-netto'),
            "dosis" : request.POST.get('u-dosis'),
            "aturan-pakai" : request.POST.get('u-aturan-pakai'),
            "kontraindikasi" : request.POST.get('u-kontraindikasi'),
            "btk-kesediaan" : request.POST.get('u-btk-kesediaan')
    }
   
    query = """
    UPDATE OBAT
    SET id_merk_obat='%s', netto='%s', dosis='%s', aturan_pakai='%s', kontraindikasi='%s', bentuk_kesediaan='%s'
    WHERE id_obat = '%s';
    """%(obat['id-merk-obat'], obat['netto'], obat['dosis'], obat['aturan-pakai'], obat['kontraindikasi'], obat['btk-kesediaan'], id)

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)
    
    return redirect('obat:mydaftarobat')

def delete_obat(request, id):
    query ="""
    DELETE FROM PRODUK 
    WHERE id_produk = '%s';
    """ %(id)
    
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('obat:mydaftarobat')
    

# converts sql rows to a dict
def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_id_obat():
    query = """
    SELECT MAX(CAST(id_obat AS integer))+1
    FROM OBAT;
	"""
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    id_obat_baru = int(cursor.fetchone()[0])
    return id_obat_baru

def get_id_produk():
    #TODO
    query = """
    SELECT CONCAT('pdd', CAST (last as integer)+1)
    FROM ( SELECT SUBSTRING(id_produk FROM 4 ) as last FROM PRODUK
    ORDER BY id_produk DESC LIMIT 1) A;
    """
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    id_produk_baru = str(cursor.fetchone()[0])

    query = """
    INSERT INTO PRODUK(id_produk)
    VALUES ('%s')
    """%(str(id_produk_baru))
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return id_produk_baru

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results
