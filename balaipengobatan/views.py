from django.shortcuts import render, redirect
from django.db import connection
from django.db import IntegrityError
from django.contrib import messages

# Create your views here.
def daftar_balai_pengobatan_view(request):
    query = """
	SELECT B.id_balai, B.alamat_balai, B.nama_balai, B.jenis_balai, B.telepon_balai, STRING_AGG(A.nama_apotek, ',') AS nama_apotek
	FROM BALAI_PENGOBATAN B JOIN BALAI_APOTEK BA
    ON B.id_balai = BA.id_balai
    JOIN APOTEK A
    ON BA.id_apotek = A.id_apotek
    GROUP BY 1
    ORDER BY 1 ;
	"""
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_balai_pengobatan = fetch(cursor)
    
    for bp in data_balai_pengobatan:
        bp['nama_apotek'] = bp['nama_apotek'].split(',')

    query = """
	SELECT id_apotek, nama_apotek
	FROM APOTEK;
	"""
    cursor.execute(query)
    data_id_apotek = fetch(cursor)

    context = {
        'data_balai_pengobatan':data_balai_pengobatan,
        'data_id_apotek':data_id_apotek,
    }
    return render(request, 'daftarbp.html', context)

def form_buat_balai_pengobatan_view(request):
    query = """
	SELECT id_apotek, nama_apotek
	FROM APOTEK;
	"""
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_id_apotek = fetch(cursor)

    return render(request, 'buatbp.html', {'data_id_apotek':data_id_apotek})

def buat_bp(request):
    bp = {
            "id-balai" : get_id_bp(),
            "alamat-balai" : request.POST.get('alamatb'),
            "nama-balai" : request.POST.get('namab'),
            "jenis-balai" : request.POST.get('jenisb'),
            "telepon-balai" : request.POST.get('telpb'),
            "daftar-id-apotek" : request.POST.getlist('idap'),
    }

    if len(bp['daftar-id-apotek']) == 0:
        messages.error(request, 'Please choose ID Apotek Berasosiasi')
        
        return redirect('balaipengobatan:myformbuatbp')

    query =  """
    INSERT INTO BALAI_PENGOBATAN (id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai)
    VALUES ('%s', '%s', '%s', '%s', '%s');
    """ %(bp['id-balai'], bp['alamat-balai'], bp['nama-balai'], bp['jenis-balai'], bp['telepon-balai'])
    
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    for idap in bp['daftar-id-apotek']:
        query =  """
        INSERT INTO BALAI_APOTEK (id_balai, id_apotek)
        VALUES ('%s', '%s');
        """ %(bp['id-balai'], idap)
        cursor.execute(query)

    return redirect('balaipengobatan:mydaftarbp')

def update_bp(request, id):
    bp = {
            "id-balai" : request.POST.get('bp-id-balai'),
            "alamat-balai" : request.POST.get('bp-alamat-balai'),
            "nama-balai" : request.POST.get('bp-nama-balai'),
            "jenis-balai" : request.POST.get('bp-jns-balai'),
            "telepon-balai" : request.POST.get('bp-telp-balai'),
            "daftar-id-apotek-baru" : request.POST.getlist('bp-id-apotek'),
    }

    if len(bp['daftar-id-apotek-baru']) == 0:
        messages.error(request, 'UPDATE ERROR : Please choose ID Apotek Berasosiasi')
        return redirect('balaipengobatan:mydaftarbp')

    query = """
    UPDATE BALAI_PENGOBATAN
    SET alamat_balai='%s', nama_balai='%s', jenis_balai='%s', telepon_balai='%s'
    WHERE id_balai = '%s';
    """%(bp['alamat-balai'], bp['nama-balai'], bp['jenis-balai'], bp['telepon-balai'], id)

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)
    
    qidlama =  """
    SELECT STRING_AGG(id_apotek, ',') AS id_apotek
    FROM BALAI_APOTEK
    WHERE id_balai = '%s';
    """%(id)

    cursor.execute(qidlama)
    data_id_apotek_lama = fetch(cursor)

    for idlama in data_id_apotek_lama:
        idlama['id_apotek'] = idlama['id_apotek'].split(',')
        for idl in idlama['id_apotek'] :
            query =  """
            DELETE FROM BALAI_APOTEK
            WHERE id_balai = '%s' AND id_apotek = '%s';
            """%(id, idl)
            cursor.execute(query)

    for idap in bp['daftar-id-apotek-baru']:
        qinsert =  """
        INSERT INTO BALAI_APOTEK (id_balai, id_apotek)
        VALUES ('%s', '%s');
        """ %(id, idap)
        cursor.execute(qinsert)

    return redirect('balaipengobatan:mydaftarbp')

def delete_bp(request, id):
    query ="""
    DELETE FROM BALAI_PENGOBATAN 
    WHERE id_balai = '%s';
    """ %(id)

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('balaipengobatan:mydaftarbp')

# converts sql rows to a dict
def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_id_bp():
    query = """
    SELECT CONCAT('bp0', CAST (last as numeric)+1)
    FROM ( SELECT SUBSTRING(id_balai FROM 3) as last FROM BALAI_PENGOBATAN
    ORDER BY id_balai DESC LIMIT 1) A;
    """
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    id_bp_baru = str(cursor.fetchone()[0])
    return id_bp_baru
