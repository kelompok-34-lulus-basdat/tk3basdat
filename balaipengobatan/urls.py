from django.urls import path
from . import views

app_name = "balaipengobatan"

urlpatterns = [
    path('daftarbp/', views.daftar_balai_pengobatan_view, name='mydaftarbp'),
    path('formbuatbp/', views.form_buat_balai_pengobatan_view, name='myformbuatbp'),
    path('buatbp/', views.buat_bp, name='mybuatbp'),
    path('updatebp/<id>', views.update_bp, name='myupdatebp'),
    path('deletebp/<id>', views.delete_bp, name='mydeletebp<id>'),
]
