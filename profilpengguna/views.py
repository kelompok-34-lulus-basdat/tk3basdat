from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.db import connection


# Create your views here.
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
def admin_apotek_view(request):
    email = request.session.get("email")    
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to FARMAKAMI;")
        cursor.execute("SELECT email, password, nama_lengkap, telepon"
                        " FROM PENGGUNA WHERE email = '{}';".format(email))
        data_profil = cursor.fetchone()
        print(data_profil)
        args = {
            'data_profil' : data_profil,
            }
        return render(request, 'profil-pengguna-adminapotek.html', args)
def konsumen_view(request):
    email = request.session.get("email")   
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to FARMAKAMI;")
        cursor.execute("SELECT P.email, P.password, P.nama_lengkap, P.telepon, K.jenis_kelamin, K.tanggal_lahir, "
                        "STRING_AGG(A.status,',') AS status, STRING_AGG(A.alamat, ';') AS alamat "
                        " FROM PENGGUNA P, Konsumen K, ALAMAT_KONSUMEN A WHERE A.id_konsumen = K.id_konsumen AND P.email = K.email AND K.email = '{}' " 
                        "GROUP BY A.id_konsumen, P.email, P.nama_lengkap, K.jenis_kelamin, K.tanggal_lahir ;".format(email))
        data_profil = dictfetchall(cursor)[0]
        print(data_profil)
        data_profil['status'] = data_profil['status'].split(',')
        data_profil['alamat'] = data_profil['alamat'].split(';')
        args = {
            'data_profil' : data_profil,
            }
        return render(request, 'profil-pengguna-konsumen.html', args)
def kurir_view(request):
    # return render(request, 'profil-pengguna-konsumen.html', args)
    email = request.session.get("email")
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to FARMAKAMI;")
        cursor.execute("SELECT P.email, P.password, P.nama_lengkap, P.telepon, K.nama_perusahaan"
                        " FROM PENGGUNA P, KURIR K WHERE P.email = K.email AND K.email = '{}';".format(email))
        data_profil = dictfetchall(cursor)[0]
        args = {
            'data_profil' : data_profil,
        }
        return render(request, 'profil-pengguna-kurir.html', args)

def cs_view(request):
    email = request.session.get("email")    
    with connection.cursor() as cursor:
        cursor.execute("SET search_path to FARMAKAMI;")
        cursor.execute("SELECT P.email, P.password, P.nama_lengkap, P.telepon, C.no_sia, C.no_ktp "
                        " FROM PENGGUNA P, CS C WHERE P.email = C.email AND C.email = '{}';".format(email))
        data_profil = dictfetchall(cursor)[0]
        args = {
            'data_profil' : data_profil,
            }
        return render(request, 'profil-pengguna-cs.html', args)
def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return[dict(zip(columns, row)) for row in cursor.fetchall()]
def profil_view(request):
    roleNow = request.session.get("role")
    if roleNow == None:
        return HttpResponse("Please Login")
    if roleNow == "ADMIN_APOTEK":
        return redirect('profilpengguna:profiladminapotek')
    elif roleNow == "CS":
        return redirect('profilpengguna:profilcs')
    elif roleNow == "KURIR":
        return redirect('profilpengguna:profilkurir')
    elif roleNow == "KONSUMEN":
       return redirect('profilpengguna:profilkonsumen')
 