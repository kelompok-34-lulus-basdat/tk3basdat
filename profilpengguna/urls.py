from django.urls import path
from . import views

app_name = "profilpengguna"

urlpatterns = [
    path('', views.profil_view, name='profil'),
    path('profile-adminapotek/', views.admin_apotek_view, name='profiladminapotek'),
    path('profile-konsumen/', views.konsumen_view, name='profilkonsumen'),
    path('profile-kurir/', views.kurir_view, name='profilkurir'),
    path('profile-cs/', views.cs_view, name='profilcs'),
]