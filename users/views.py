from django.shortcuts import render, redirect
from django.contrib.auth import authenticate
from .forms import LoginForm
from .forms import RegAdminForm, RegCsForm, RegKurirForm, RegKonsumenForm
from django.db import connection
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.contrib import messages

# Create your views here.
def fetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]
# def login_view(request):
#     return render(request, 'login.html')

# def register_view(request):
#     return render(request, 'register.html')

def home(request):
    if request.session.is_empty():
        return render(request, 'homepage:myhomepage')
    else:
        return render(request, 'homepage:landingpage')

# def register_view(request):
#     return render(request, 'register.html')

#def konsumen_form_view(request):
#    return render(request, 'konsumen_form.html')

# def kurir_form_view(request):
#     return render(request, 'kurir_form.html')

# def cs_form_view(request):
#     return render(request, 'cs_form.html')

#----------------LOGIN----------------#
def user_login(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = LoginForm(request.POST)
            if form.is_valid():
                emails = form.cleaned_data["email"]
                password = form.cleaned_data["password"]

                cursor.execute("SET SEARCH_PATH TO FARMAKAMI;")
                cursor.execute("SELECT * FROM PENGGUNA WHERE " + "email = %s AND password = %s;", [emails, password])
             
                user = fetchall(cursor)
                
                if len(user) != 0:
                    user = user[0]
                    email = user["email"]
                    request.session["email"] = email
                    request.session["nama_lengkap"] = user["nama_lengkap"]

                    if check_role(email, "KONSUMEN"):
                        request.session["role"] = "KONSUMEN"
                    elif check_role(email, "ADMIN_APOTEK"):
                        request.session["role"] = "ADMIN_APOTEK"
                    elif check_role(email, "KURIR"):
                        request.session["role"] = "KURIR"
                    elif check_role(email, "CS"):
                        request.session["role"] = "CS"

                    roleNow = request.session["role"]
                    print(roleNow)
                    if roleNow == "ADMIN_APOTEK":
                        return redirect('profilpengguna:profiladminapotek')
                    elif roleNow == "CS":
                        return redirect('profilpengguna:profilcs')
                    elif roleNow == "KURIR":
                        return redirect('profilpengguna:profilkurir')
                    elif roleNow == "KONSUMEN":
                        return redirect('profilpengguna:profilkonsumen')

                messages.error(request, 'Your email and/or password do not match')
                return redirect('users:mylogin')
           
        else:
            
            form = LoginForm()
            return render(request, "login.html", {'form':form})

def check_role(email, role):
    with connection.cursor() as cursor:
        cursor.execute("SET SEARCH_PATH TO FARMAKAMI;")
        query = "SELECT * FROM " + role + " WHERE email = %s;"
        cursor.execute(query, [email])
        row = fetchall(cursor)

        if len(row) != 0:
            return True
        else:
            return False

#--------LOGOUT----------#
# def userLogout(request):
#     # request.session.flush()
#     return redirect('homepage:homepage')
#----------------REGISTER ADMIN----------------#
def register_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegAdminForm(request.POST)
            if form.is_valid():
                email = request.POST.get('email')
                print(email)
                password = request.POST.get('password')
                nama_lengkap = request.POST.get('nama_lengkap')
                no_telepon = request.POST.get('no_telepon')

                id_apotek = 'apd01'
                #Insert into PENGGUNA 
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO PENGGUNA(email, password, nama_lengkap, telepon)"
                                "VALUES ('{}', '{}', '{}', '{}');".format(email,password,nama_lengkap,no_telepon))

                #Insert into APOTEKER
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO APOTEKER(email) VALUES ('{}');".format(email))

                #Insert into ADMIN_APOTEK
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO ADMIN_APOTEK (email,id_apotek) VALUES('{}', '{}');".format(email,id_apotek))

                request.session['email'] = email
                request.session["role"] = "ADMIN_APOTEK"
                return redirect('profilpengguna:profiladminapotek')
    form = RegAdminForm()
    args = {
        'form' : form,
    }
    return render(request, 'register.html', args)

#----------------REGISTER KONSUMEN----------------#
def konsumen_form_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegKonsumenForm(request.POST)
            if form.is_valid():
                email = request.POST.get('email')
                password = request.POST.get('password')
                nama_lengkap = request.POST.get('nama_lengkap')
                no_telepon = request.POST.get('no_telepon')
                jenis_kelamin = request.POST.get('jenis_kelamin')
                tgl_lahir = request.POST.get('tgl_lahir')
                status_alamat1 = request.POST.get('status_alamat1')
                alamat1  = request.POST.get('alamat1')
                status_alamat2 = request.POST.get('status_alamat2')
                alamat2  = request.POST.get('alamat2')
                status_alamat3 = request.POST.get('status_alamat3')
                alamat3  = request.POST.get('alamat3')
                dictalamat = {
                    "alamatkey1" : ["alamat1", "status_alamat1"],
                    "alamatkey2" : ["alamat2", "status_alamat2"],
                    "alamatkey3" : ["alamat3", "status_alamat3"],
                }
                id_konsumen = get_id_konsumen()

                #Insert into PENGGUNA 
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO PENGGUNA(email, password, nama_lengkap, telepon)"
                                "VALUES ('{}', '{}', '{}', '{}');".format(email,password,nama_lengkap,no_telepon))

                #Insert into KONSUMEN
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO KONSUMEN(id_konsumen, email, jenis_kelamin, tanggal_lahir)"
                                "VALUES ('{}', '{}', '{}', '{}');".format(id_konsumen, email, jenis_kelamin, tgl_lahir))

                #Insert into ALAMAT
                for da in dictalamat :
                    da['alamatkey1'] = da['alamatkey1'].split(',')
                    da['alamatkey2'] = st['alamatkey2'].split(',')
                    da['alamatkey3'] = st['alamatkey3'].split(',')
                    
                    qinsert =  """
                    INSERT INTO ALAMAT_KONSUMEN (id_konsumen, alamat, status)
                    VALUES ('%s', '%s', '%s');
                    """ %(id_konsumen, da[0], da[1])
                    cursor.execute(qinsert)

                request.session['email'] = email
                request.session["role"] = "KONSUMEN"
                return redirect('profilpengguna:profilkonsumen')
    form = RegKonsumenForm()
    args = {
        'form' : form,
    }
    return render(request, 'konsumen_form.html', args)

def get_id_konsumen():
    query = """
    SELECT CONCAT('kd0', CAST (last as integer)+1)
    FROM ( SELECT SUBSTRING(id_konsumen FROM 3 ) as last FROM KONSUMEN
    ORDER BY id_konsumen DESC LIMIT 1) A;
    """
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)
    id_konsumen_baru = str(cursor.fetchone()[0])

    return id_konsumen_baru

#---------- REGIS CS -----------#
def cs_form_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegCsForm(request.POST)
            if form.is_valid():
                email = request.POST.get('email')
                password = request.POST.get('password')
                nama_lengkap = request.POST.get('nama_lengkap')
                no_telepon = request.POST.get('no_telepon')
                no_ktp = request.POST.get('no_ktp')
                no_sia = request.POST.get('no_sia')
               
                #Insert into PENGGUNA 
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO PENGGUNA(email, password, nama_lengkap, telepon)"
                                "VALUES ('{}', '{}', '{}', '{}');".format(email,password,nama_lengkap,no_telepon))

                #Insert into APOTEKER
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO APOTEKER(email) VALUES ('{}');".format(email))

                #Insert into CS
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO CS(no_ktp,email,no_sia) VALUES ('{}', '{}', '{}');".format(no_ktp,email,no_sia))


                request.session['email'] = email
                request.session["role"] = "CS"
                return redirect('profilpengguna:profilcs')
    form = RegCsForm()
    args = {
        'form' : form,
    }
    return render(request, 'cs_form.html', args)
#-------------------------------#
#-------REG KURIR --------------#
def kurir_form_view(request):
    with connection.cursor() as cursor:
        if request.method == 'POST':
            form = RegKurirForm(request.POST)
            if form.is_valid():
                email = request.POST.get('email')
                password = request.POST.get('password')
                nama_lengkap = request.POST.get('nama_lengkap')
                no_telepon = request.POST.get('no_telepon')
                nama_perusahaan = request.POST.get('nama_perusahaan')
                cursor.execute("SELECT COUNT(*) AS jumlah_kurir FROM KURIR")
                id_kurir = 'krd' +  str(int((dictfetchall(cursor)[0])['jumlah_kurir']) + 1) 
                #Insert into PENGGUNA 
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO PENGGUNA(email, password, nama_lengkap, telepon)"
                                "VALUES ('{}', '{}', '{}', '{}');".format(email,password,nama_lengkap,no_telepon))

                #Insert into KURIR
                cursor.execute("SET search_path TO farmakami;")
                cursor.execute("INSERT INTO KURIR(id_kurir,email, nama_perusahaan)"
                                "VALUES ('{}', '{}', '{}');".format(id_kurir,email,nama_perusahaan))
                request.session['email'] = email
                request.session["role"] = "KURIR"
                return redirect('profilpengguna:profilkurir')
    form = RegKurirForm()
    args = {
        'form' : form,
    }
    return render(request, 'kurir_form.html', args)
def user_logout(request):
    request.session.flush()
    return redirect('homepage:myhomepage')
#-------------------#
def kurir_view(request):
    email = request.session.get("email")
    role = request.session.get("role")
    if email == None:
        return HttpResponse("email kosong ", email)     
    else:

        with connection.cursor() as cursor:
            cursor.execute("SET search_path to FARMAKAMI;")
            cursor.execute("SELECT P.email, P.password, P.nama_lengkap, P.telepon, K.nama_perusahaan"
                            " FROM PENGGUNA P, KURIR K WHERE P.email = K.email AND K.email = '{}';".format(email))
            data_profil = dictfetchall(cursor)[0]
            args = {
                'data_profil' : data_profil,
                }
            return render(request, 'profil-pengguna-kurir.html', args)