from django import forms
from django.db import connection
from django.core.paginator import Paginator
# from django.core.validator import RegexValidator

#----------------LOGIN----------------#
class LoginForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

#----------------REGISTER ADMIN----------------#
class RegAdminForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    nama_lengkap = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class':'form-control'}))
    no_telepon = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={'class':'form-control'}))

class RegKonsumenForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    nama_lengkap = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class':'form-control'}))
    no_telepon = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={'class':'form-control'}))
    jenis_kelamin = forms.CharField(label='Jenis Kelamin', widget=forms.TextInput(attrs={'class':'form-control'}))
    tgl_lahir = forms.DateField(
        label='Tanggal Lahir', 
        widget=forms.DateInput(
            attrs={'class':'form-control', 'type':'date'}
            )
        )
    status_alamat1 = forms.CharField(label='Status Alamat', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Status alamat 1' }))
    alamat1 = forms.CharField(label='Alamat', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Alamat 1'}))
    status_alamat2 = forms.CharField(label='Status Alamat', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Status alamat 2' }))
    alamat2 = forms.CharField(label='Alamat', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Alamat 2'}))
    status_alamat3 = forms.CharField(label='Status Alamat', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Status alamat 3' }))
    alamat3 = forms.CharField(label='Alamat', widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Alamat 3'}))

class RegCsForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    nama_lengkap = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class':'form-control'}))
    no_telepon = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={'class':'form-control'}))
    no_ktp = forms.CharField(label='No KTP', widget=forms.TextInput(attrs={'class':'form-control'}))
    no_sia = forms.CharField(label='No SIAK', widget=forms.TextInput(attrs={'class':'form-control'}))

class RegKurirForm(forms.Form):
    email = forms.CharField(label='Email', widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    nama_lengkap = forms.CharField(label='Nama Lengkap', widget=forms.TextInput(attrs={'class':'form-control'}))
    no_telepon = forms.CharField(label='No Telepon', widget=forms.TextInput(attrs={'class':'form-control'}))
    nama_perusahaan = forms.CharField(label='Nama perusahaan', widget=forms.TextInput(attrs={'class':'form-control'}))

    
