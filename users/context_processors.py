def add_variable_to_context(request):
    return {
        'email': request.session.get('email'),
        'role': request.session.get('role')
    }