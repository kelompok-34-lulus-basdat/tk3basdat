from django.urls import path
from . import views

app_name = "users"

urlpatterns = [
    path('login/', views.user_login, name='mylogin'),
    path('register/adminapotek/', views.register_view, name='myregister'),
    path('register/konsumen/', views.konsumen_form_view, name='mykonsumenform'),
    path('register/kurir/', views.kurir_form_view, name='mykurirform'),
    path('register/cs/', views.cs_form_view, name='mycsform'),
    path('', views.home, name='home'),
    path('profile-kurir/', views.kurir_view, name='profilkurir'),
    path('logout/', views.user_logout, name='logout'),
]
