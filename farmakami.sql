--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;

--
-- Name: farmakami; Type: SCHEMA; Schema: -; Owner: dbi34
--

CREATE SCHEMA farmakami;


ALTER SCHEMA farmakami OWNER TO dbi34;

--
-- Name: readbp(); Type: FUNCTION; Schema: farmakami; Owner: dbi34
--

CREATE FUNCTION farmakami.readbp() RETURNS TABLE(id_balai character varying, alamat_balai text, nama_balai character varying, jenis_balai character varying, telepon_balai character varying)
    LANGUAGE plpgsql
    AS $$
BEGIN
RETURN QUERY SELECT * FROM balai_pengobatan;
END;
$$;


ALTER FUNCTION farmakami.readbp() OWNER TO dbi34;

--
-- Name: ubah_kurir(); Type: FUNCTION; Schema: farmakami; Owner: dbi34
--

CREATE FUNCTION farmakami.ubah_kurir() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
IF(OLD.status_pengiriman !=
'sedang diproses' OR OLD.status_pengiriman !='sudah terkirim')
THEN
UPDATE PENGANTARAN_FARMASI
SET status_pengiriman =
'sedang diproses'
WHERE id_transaksi = NEW.id_transaksi;
RETURN NEW;
END IF;
END;
$$;


ALTER FUNCTION farmakami.ubah_kurir() OWNER TO dbi34;

--
-- Name: update_stok_produk(); Type: FUNCTION; Schema: farmakami; Owner: dbi34
--

CREATE FUNCTION farmakami.update_stok_produk() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
stok_produk INTEGER;
BEGIN
SELECT stok INTO stok_produk FROM PRODUK_APOTEK WHERE id_produk= NEW.id_produk AND id_apotek = NEW.id_apotek;
IF (NEW.jumlah > stok_produk) THEN
NEW.jumlah := stok_produk;
END IF;
UPDATE PRODUK_APOTEK SET stok = stok - NEW.jumlah WHERE id_produk = NEW.id_produk AND id_apotek = NEW.id_apotek;
RETURN NEW;
END;
$$;


ALTER FUNCTION farmakami.update_stok_produk() OWNER TO dbi34;

--
-- Name: update_total_biaya_pengantaran(); Type: FUNCTION; Schema: farmakami; Owner: dbi34
--

CREATE FUNCTION farmakami.update_total_biaya_pengantaran() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
    total_harga INTEGER;
BEGIN
    SELECT total_pembayaran INTO total_harga
    FROM TRANSAKSI_PEMBELIAN
    WHERE id_transaksi_pembelian = NEW.id_transaksi_pembelian;

    UPDATE PENGANTARAN_FARMASI
    SET total_biaya = total_harga + NEW.biaya_kirim
    WHERE id_pengantaran = NEW.id_pengantaran;

RETURN NEW;
END;
$$;


ALTER FUNCTION farmakami.update_total_biaya_pengantaran() OWNER TO dbi34;

--
-- Name: update_total_pembayaran(); Type: FUNCTION; Schema: farmakami; Owner: dbi34
--

CREATE FUNCTION farmakami.update_total_pembayaran() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
harga_produk INTEGER;
total_harga INTEGER;
BEGIN
SELECT harga_jual INTO harga_produk 
FROM PRODUK_APOTEK as PA
WHERE NEW.id_apotek = PA.id_apotek 
AND NEW.id_produk = PA.id_produk;
IF(TG_OP = 'INSERT') THEN
total_harga = NEW.jumlah * harga_produk;
END IF;
UPDATE TRANSAKSI_PEMBELIAN
SET total_pembayaran = total_pembayaran + total_harga
WHERE NEW.id_transaksi_pembelian = id_transaksi_pembelian;
RETURN NEW;
END;
$$;


ALTER FUNCTION farmakami.update_total_pembayaran() OWNER TO dbi34;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_apotek; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.admin_apotek (
    email character varying(50) NOT NULL,
    id_apotek character varying(10)
);


ALTER TABLE farmakami.admin_apotek OWNER TO dbi34;

--
-- Name: alamat_konsumen; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.alamat_konsumen (
    id_konsumen character varying(10) NOT NULL,
    alamat text NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE farmakami.alamat_konsumen OWNER TO dbi34;

--
-- Name: alat_medis; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.alat_medis (
    id_alat_medis character varying(10) NOT NULL,
    nama_alat_medis character varying(50) NOT NULL,
    deskripsi_alat text,
    jenis_penggunaan character varying(20) NOT NULL,
    id_produk character varying(10) NOT NULL
);


ALTER TABLE farmakami.alat_medis OWNER TO dbi34;

--
-- Name: apotek; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.apotek (
    id_apotek character varying(10) NOT NULL,
    email character varying(50) NOT NULL,
    no_sia character varying(20) NOT NULL,
    nama_penyelenggara character varying(50) NOT NULL,
    nama_apotek character varying(50) NOT NULL,
    alamat_apotek text NOT NULL,
    telepon_apotek character varying(20)
);


ALTER TABLE farmakami.apotek OWNER TO dbi34;

--
-- Name: apoteker; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.apoteker (
    email character varying(50) NOT NULL
);


ALTER TABLE farmakami.apoteker OWNER TO dbi34;

--
-- Name: balai_apotek; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.balai_apotek (
    id_balai character varying(10) NOT NULL,
    id_apotek character varying(10) NOT NULL
);


ALTER TABLE farmakami.balai_apotek OWNER TO dbi34;

--
-- Name: balai_pengobatan; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.balai_pengobatan (
    id_balai character varying(10) NOT NULL,
    alamat_balai text NOT NULL,
    nama_balai character varying(50) NOT NULL,
    jenis_balai character varying(30) NOT NULL,
    telepon_balai character varying(20)
);


ALTER TABLE farmakami.balai_pengobatan OWNER TO dbi34;

--
-- Name: cs; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.cs (
    no_ktp character varying(20) NOT NULL,
    email character varying(50) NOT NULL,
    no_sia character varying(20) NOT NULL
);


ALTER TABLE farmakami.cs OWNER TO dbi34;

--
-- Name: kandungan; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.kandungan (
    id_bahan character varying(10) NOT NULL,
    id_obat character varying(10) NOT NULL,
    takaran character varying(10) NOT NULL
);


ALTER TABLE farmakami.kandungan OWNER TO dbi34;

--
-- Name: kategori_merk_obat; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.kategori_merk_obat (
    id_kategori character varying(10) NOT NULL,
    id_merk_obat character varying(10) NOT NULL
);


ALTER TABLE farmakami.kategori_merk_obat OWNER TO dbi34;

--
-- Name: kategori_obat; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.kategori_obat (
    id_kategori character varying(10) NOT NULL,
    nama_kategori character varying(50) NOT NULL,
    perusahaan text
);


ALTER TABLE farmakami.kategori_obat OWNER TO dbi34;

--
-- Name: komposisi; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.komposisi (
    id_bahan character varying(10) NOT NULL,
    nama_bahan character varying(50) NOT NULL
);


ALTER TABLE farmakami.komposisi OWNER TO dbi34;

--
-- Name: konsumen; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.konsumen (
    id_konsumen character varying(10) NOT NULL,
    email character varying(50) NOT NULL,
    jenis_kelamin character varying(1) NOT NULL,
    tanggal_lahir date NOT NULL
);


ALTER TABLE farmakami.konsumen OWNER TO dbi34;

--
-- Name: kurir; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.kurir (
    id_kurir character varying(10) NOT NULL,
    email character varying(50) NOT NULL,
    nama_perusahaan character varying(50) NOT NULL
);


ALTER TABLE farmakami.kurir OWNER TO dbi34;

--
-- Name: list_produk_dibeli; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.list_produk_dibeli (
    jumlah integer NOT NULL,
    id_apotek character varying(10) NOT NULL,
    id_produk character varying(10) NOT NULL,
    id_transaksi_pembelian character varying(10) NOT NULL
);


ALTER TABLE farmakami.list_produk_dibeli OWNER TO dbi34;

--
-- Name: merk_dagang; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.merk_dagang (
    id_merk character varying(10) NOT NULL,
    nama_merk character varying(50) NOT NULL,
    perusahaan character varying(50) NOT NULL
);


ALTER TABLE farmakami.merk_dagang OWNER TO dbi34;

--
-- Name: merk_dagang_alat_medis; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.merk_dagang_alat_medis (
    id_alat_medis character varying(10) NOT NULL,
    id_merk character varying(10) NOT NULL,
    fitur text NOT NULL,
    dimensi character varying(10) NOT NULL,
    cara_penggunaan text,
    model character varying(20)
);


ALTER TABLE farmakami.merk_dagang_alat_medis OWNER TO dbi34;

--
-- Name: merk_obat; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.merk_obat (
    id_merk_obat character varying(10) NOT NULL,
    golongan character varying(25) NOT NULL,
    nama_dagang character varying(50) NOT NULL,
    perusahaan character varying(50) NOT NULL
);


ALTER TABLE farmakami.merk_obat OWNER TO dbi34;

--
-- Name: obat; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.obat (
    id_obat character varying(10) NOT NULL,
    id_produk character varying(10) NOT NULL,
    id_merk_obat character varying(10) NOT NULL,
    netto character varying(10) NOT NULL,
    dosis text NOT NULL,
    aturan_pakai text,
    kontradiksi text,
    bentuk_ketersediaan text NOT NULL
);


ALTER TABLE farmakami.obat OWNER TO dbi34;

--
-- Name: pengantaran_farmasi; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.pengantaran_farmasi (
    id_pengantaran character varying(10) NOT NULL,
    id_kurir character varying(10) NOT NULL,
    id_transaksi_pembelian character varying(10),
    waktu timestamp without time zone NOT NULL,
    status_pengiriman character varying(20) NOT NULL,
    biaya_kirim integer NOT NULL,
    total_biaya integer NOT NULL
);


ALTER TABLE farmakami.pengantaran_farmasi OWNER TO dbi34;

--
-- Name: pengguna; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.pengguna (
    email character varying(50) NOT NULL,
    telepon character varying(20),
    password character varying(128) NOT NULL,
    nama_lengkap character varying(50) NOT NULL
);


ALTER TABLE farmakami.pengguna OWNER TO dbi34;

--
-- Name: produk; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.produk (
    id_produk character varying(10) NOT NULL
);


ALTER TABLE farmakami.produk OWNER TO dbi34;

--
-- Name: produk_apotek; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.produk_apotek (
    harga_jual integer NOT NULL,
    stok integer NOT NULL,
    satuan_penjualan character varying(5) NOT NULL,
    id_produk character varying(10) NOT NULL,
    id_apotek character varying(10) NOT NULL
);


ALTER TABLE farmakami.produk_apotek OWNER TO dbi34;

--
-- Name: transaksi_dengan_resep; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.transaksi_dengan_resep (
    no_resep character varying(20) NOT NULL,
    nama_dokter character varying(50) NOT NULL,
    nama_pasien character varying(50) NOT NULL,
    isi_resep text NOT NULL,
    unggahan_resep text,
    tanggal_resep date NOT NULL,
    status_validasi character varying(20) NOT NULL,
    no_telepon character varying(20),
    id_transaksi_pembelian character varying(10) NOT NULL,
    email_apoteker character varying(50)
);


ALTER TABLE farmakami.transaksi_dengan_resep OWNER TO dbi34;

--
-- Name: transaksi_online; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.transaksi_online (
    no_urut_pembelian character varying(20) NOT NULL,
    id_transaksi_pembelian character varying(5)
);


ALTER TABLE farmakami.transaksi_online OWNER TO dbi34;

--
-- Name: transaksi_pembelian; Type: TABLE; Schema: farmakami; Owner: dbi34; Tablespace: 
--

CREATE TABLE farmakami.transaksi_pembelian (
    id_transaksi_pembelian character varying(10) NOT NULL,
    waktu_pembelian timestamp without time zone NOT NULL,
    total_pembayaran integer NOT NULL,
    id_konsumen character varying(10) NOT NULL
);


ALTER TABLE farmakami.transaksi_pembelian OWNER TO dbi34;

--
-- Data for Name: admin_apotek; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.admin_apotek (email, id_apotek) FROM stdin;
joret1@aboutads.info	apd01
clethleyb@ox.ac.uk	apd02
tmacpheel@webeden.co.uk	apd03
lhusband8@chronoengine.com	apd04
dbeeswingi@thetimes.co.uk	apd05
\.


--
-- Data for Name: alamat_konsumen; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.alamat_konsumen (id_konsumen, alamat, status) FROM stdin;
kd01	JL. Tebet Raya No. 84, Tebet, Jakarta Selatan	Alamat rumah
kd01	Jl. KH Agus Salim 16, Sabang, Menteng, Jakarta Pusat	Alamat kantor
kd02	Jl. Kepu Dalam 7, Kemayoran, Jakarta Pusat, Kemayoran, Jakarta Pusat, DKI Jakarta	Alamat rumah
kd02	Jl. Batu Jajar no. 11A, Sawah Besar, Jakarta Pusat, Indonesia	Alamat kantor
kd03	Jalan Suka Damai V, Bintaro, Jakarta Selatan, DKI Jakarta	Alamat rumah
kd03	Ruko Emerald Avenue I Blok EA/A07, Jalan Boulevard Bintaro Jaya, Sektor IX, Tangerang Selatan	Alamat kantor
kd03	Jl. Bintaro Permai Raya, RT.5/RW.3, Pesanggrahan, Jakarta Selatan, DKI Jakarta	Alamat apartemen
kd04	Perumahan Griya Agung Permai. Jl. Sumur Batu Raya Blok C No. 30-31, Cempaka Baru  Kemayoran, Jakarta Pusat	Alamat rumah
kd04	Jl. Gunung Sahari XI No. 24 RT 3/3, Jakarta Pusat	Alamat kos
kd05	Jl.Cirendeu Raya, Lebak Bulus, Jakarta Selatan, DKI Jakarta	Alamat rumah
\.


--
-- Data for Name: alat_medis; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.alat_medis (id_alat_medis, nama_alat_medis, deskripsi_alat, jenis_penggunaan, id_produk) FROM stdin;
AM001	thermometer	alat pengukur suhu tubuh	luar	pdd01
AM002	kruk	alat bantu jalan	luar	pdd20
AM003	kursi roda	alat bantu jalan	luar	pdd03
AM004	tabung oksigen	media suplai oksigen	luar	pdd06
AM005	regulator oksigen	regulasi alat bantu pernapasan	luar	pdd12
AM006	desinfektan	cairan pembersih tangan	luar	pdd15
AM007	reagen	cairan pereaksi untuk darah	luar	pdd04
AM008	stetoskop	alat medis akustik untuk memeriksa suara dalam tubuh	luar	pdd01
AM009	kapas	bahan utama untuk membersihkan luka	luar	pdd09
AM010	kain kasa	kain untuk membalut luka	luar	pdd09
AM011	masker	penutup area sekitar mulut dan hidung	luar	pdd09
AM012	headcap	penutup kepala untuk membungkus rambut	luar	pdd16
AM013	handscoon	sarung tangan medis	luar	pdd15
AM014	N95	penutup area sekitar mulut dan hidung	luar	pdd14
AM015	N88	penutup area sekitar mulut dan hidung	luar	pdd17
\.


--
-- Data for Name: apotek; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.apotek (id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon_apotek) FROM stdin;
apd01	apotek.antasari@gmail.com	100292258.0	Kellen Gingle	Apotek Antasari	Jl. Aipda KS Tubun 63, Jakarta Pusat, DKI Jakarta	215712368
apd02	bintangsehat@ymail.com	643599533.0	Ferrell Gabites	Apotek Bintang Sehat	Jl Pademangan Raya 257-A, Jakarta	2164713663
apd03	duta.esa.farma@hotmail.com	116427725.0	Mackenzie Segot	Apotek Duta Esa Farma	Jl Raya Kranggan Kompl Kranggan Permai Bl V/16 Jakarta	218440468
apd04	madagadjah@google.com	479070561.0	Lynett Gott	Apotik Gadjah Mada	Jl Gajah Mada 181 Jakarta	216292521
apd05	kelapaduaaa@yahoo.com	550043929.0	Dennis Cranmore	Apotek Kelapa Dua	Jl Akses UI 69 RT 006/06 Jakarta	2187705910
\.


--
-- Data for Name: apoteker; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.apoteker (email) FROM stdin;
joret1@aboutads.info
kgoter6@nytimes.com
clethleyb@ox.ac.uk
odrengg@va.gov
tmacpheel@webeden.co.uk
ntotaro2@webs.com
lhusband8@chronoengine.com
bpoldingn@printfriendly.com
dbeeswingi@thetimes.co.uk
pestabrooke@lulu.com
\.


--
-- Data for Name: balai_apotek; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.balai_apotek (id_balai, id_apotek) FROM stdin;
1168634032	apd01
2451090995	apd02
6598312285	apd03
6531537517	apd04
7917351783	apd05
\.


--
-- Data for Name: balai_pengobatan; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.balai_pengobatan (id_balai, alamat_balai, nama_balai, jenis_balai, telepon_balai) FROM stdin;
1168634032	Jl Gn Sahari XI Bl B-30/11, Dki Jakarta	Klinik Pratama Cilebut	Klinik	161304245127
2451090995	Jl Cipinang Indah II 24, Dki Jakarta	RS Jatinegara	Rumah Sakit	165903193711
6598312285	Jl Swasembada Brt 11/47, Dki Jakarta	Puskesmas Bbk Surabaya	Puskesmas	162012086506
6531537517	Jl KH Fachrudin Bl 8/11-13, Dki Jakarta	Puskesmas Sei Panas	Puskesmas	161112245905
7917351783	Jl Duren Tiga PLN 14, Dki Jakarta	Puskesmas Kapas	Puskesmas	169209301606
\.


--
-- Data for Name: cs; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.cs (no_ktp, email, no_sia) FROM stdin;
3276068356401801	kgoter6@nytimes.com	593118359.0
3276068427984715	odrengg@va.gov	443762921.0
3276063279887674	ntotaro2@webs.com	163068201.0
3276065959437783	bpoldingn@printfriendly.com	555237701.0
3276064286863735	pestabrooke@lulu.com	567840329.0
\.


--
-- Data for Name: kandungan; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.kandungan (id_bahan, id_obat, takaran) FROM stdin;
\.


--
-- Data for Name: kategori_merk_obat; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.kategori_merk_obat (id_kategori, id_merk_obat) FROM stdin;
1	4041952909
2	9066102337
3	1688555511
4	8171332675
5	2498489353
6	4041952909
7	9066102337
8	1688555511
9	8171332675
10	2498489353
\.


--
-- Data for Name: kategori_obat; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.kategori_obat (id_kategori, nama_kategori, perusahaan) FROM stdin;
1	1	kimia farma
2	2	jaco
3	3	generic
4	4	generic
5	5	kimia farma
6	6	generic
7	7	kimia farma
8	8	generic
9	9	kimia farma
10	10	generic
\.


--
-- Data for Name: komposisi; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.komposisi (id_bahan, nama_bahan) FROM stdin;
komp01	Magnesium Citrate
komp02	Acetaminophen and Phenylephrine HCl
komp03	AMOXICILLIN
komp04	TERCONAZOLE
komp05	ezetimibe and simvastatin
\.


--
-- Data for Name: konsumen; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.konsumen (id_konsumen, email, jenis_kelamin, tanggal_lahir) FROM stdin;
kd01	gkief0@yelp.com	M	1992-09-08
kd02	dcricket5@vimeo.com	M	1980-09-22
kd03	wgammadea@jimdo.com	M	1997-03-20
kd04	rjoyesf@tuttocitta.it	F	1983-04-15
kd05	cwetheyk@g.co	F	2000-05-10
\.


--
-- Data for Name: kurir; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.kurir (id_kurir, email, nama_perusahaan) FROM stdin;
krd01	amerrett3@alexa.com	Pos Indonesia
krd02	tfernez7@thetimes.co.uk	JNE
krd03	hboyall9@ebay.co.uk	J&T
krd04	ekibardj@twitpic.com	Lion Parcel
krd05	qrikkardo@phpbb.com	Sicepat
\.


--
-- Data for Name: list_produk_dibeli; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.list_produk_dibeli (jumlah, id_apotek, id_produk, id_transaksi_pembelian) FROM stdin;
5	apd02	pdd19	trd15
3	apd01	pdd08	trd01
8	apd03	pdd13	trd08
4	apd02	pdd19	trd12
2	apd05	pdd05	trd09
1	apd01	pdd08	trd14
2	apd04	pdd11	trd04
1	apd04	pdd11	trd02
2	apd05	pdd05	trd11
3	apd01	pdd08	trd07
\.


--
-- Data for Name: merk_dagang; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.merk_dagang (id_merk, nama_merk, perusahaan) FROM stdin;
6799114983	Betadine	PT Kalbe Farma
5882954129	Aspirin	PT Nensa Farma
9988231261	Pepsodene	Unilever
7173736535	Hansaplast	Djorge Bronson Ind
8308398573	Imboost	PT Prima Persada
7312902981	Garnier 	PT Tunas Mulia
5384823833	Tessa	PT Paseo
2278629361	Minyak Cap Kapak	PT Cap Lang
3442596140	Pampers	PT Maju Sehat
6245129507	Etanol	PT Kimia Farma
\.


--
-- Data for Name: merk_dagang_alat_medis; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.merk_dagang_alat_medis (id_alat_medis, id_merk, fitur, dimensi, cara_penggunaan, model) FROM stdin;
AM001	6799114983	mengecek batuk	87	a1	\N
AM002	5882954129	mengecek batuk	81	a1	\N
AM003	9988231261	mengecek detak jantung	84	a2	\N
AM004	7173736535	mengecek detak jantung	50	a2	\N
AM005	8308398573	mengecek suhu	77	a3	\N
AM006	7312902981	mengecek detak jantung	21	a2	\N
AM007	5384823833	mengecek suhu	44	a3	\N
AM008	2278629361	mengecek gula darah	20	a4	\N
AM009	3442596140	mengecek tekanan darah	32	a5	\N
AM010	6245129507	mengecek tekanan darah	24	a5	\N
\.


--
-- Data for Name: merk_obat; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.merk_obat (id_merk_obat, golongan, nama_dagang, perusahaan) FROM stdin;
4041952909	obat bebas	Konidin	PT Sanbe Farma
9066102337	obat bebas	Promag	PT Klimus Satu
1688555511	obat keras	Cefat	Andri Pharmaceuticals
8171332675	obat wajib apotek	Bisolvon	PT Kimia Farma
2498489353	obat wajib apotek	Neozep	PT Orins Great
\.


--
-- Data for Name: obat; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.obat (id_obat, id_produk, id_merk_obat, netto, dosis, aturan_pakai, kontradiksi, bentuk_ketersediaan) FROM stdin;
1	pdd01	4041952909	42	2kali/hari	sebelum makan	hamil,balita	cair
2	pdd02	9066102337	66	2kali/hari	sesudah makan	hamil,balita,asma	pil
3	pdd03	1688555511	13	2kali/hari	sebelum makan	diabetes,asma	pil
4	pdd04	8171332675	2	2kali/hari	sesudah makan	hamil,balita	bubuk
5	pdd05	2498489353	34	2kali/hari	sebelum makan	hamil,balita,asma	cair
6	pdd06	4041952909	57	2kali/hari	sesudah makan	diabetes,asma	cair
7	pdd07	9066102337	31	2kali/hari	sebelum makan	hamil,balita	pil
8	pdd08	1688555511	57	2kali/hari	sesudah makan	hamil,balita,asma	pil
9	pdd09	8171332675	6	2kali/hari	sebelum makan	diabetes,asma	bubuk
10	pdd10	2498489353	45	2kali/hari	sesudah makan	hamil,balita	cair
11	pdd11	4041952909	23	2kali/hari	sebelum makan	hamil,balita,asma	cair
12	pdd12	9066102337	67	2kali/hari	sesudah makan	diabetes,asma	pil
13	pdd13	1688555511	6	2kali/hari	sebelum makan	hamil,balita	pil
14	pdd14	8171332675	4	2kali/hari	sesudah makan	hamil,balita,asma	bubuk
15	pdd15	2498489353	16	2kali/hari	sesudah makan	diabetes,asma	cair
\.


--
-- Data for Name: pengantaran_farmasi; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.pengantaran_farmasi (id_pengantaran, id_kurir, id_transaksi_pembelian, waktu, status_pengiriman, biaya_kirim, total_biaya) FROM stdin;
7193251090	krd01	trd05	2019-07-23 20:52:04	sudah terkirim	133000	110000
9302052265	krd02	trd06	2018-12-18 02:20:01	sudah terkirim	89000	287000
3897360551	krd03	trd07	2019-11-22 12:48:31	sudah terkirim	100000	296000
1459617959	krd04	trd01	2018-10-23 10:44:38	sudah terkirim	61000	139000
9748568924	krd05	trd09	2020-02-13 23:23:13	sudah terkirim	61000	155000
6564540280	krd02	trd11	2019-07-29 10:58:12	sudah terkirim	85000	296000
5992311580	krd03	trd03	2019-10-05 19:26:39	sudah terkirim	87000	208000
9829588572	krd01	trd13	2019-09-07 02:37:04	belum terkirim	103000	177000
1614157596	krd02	trd14	2018-11-26 04:17:17	belum terkirim	37000	157000
4058931183	krd03	trd15	2019-08-06 02:36:01	belum terkirim	23000	220000
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.pengguna (email, telepon, password, nama_lengkap) FROM stdin;
gkief0@yelp.com	878555959	FcJIocmfj8w0	Grethel Kief
joret1@aboutads.info	854555353	XM5pEgEyud4	Jerry Oret
ntotaro2@webs.com	838555477	XnbBhxNS	Nate Totaro
amerrett3@alexa.com	838555794	jY2WzM643S9J	Aggi Merrett
salyonov4@goo.ne.jp	878555490	72Ttoaf	Sarena Alyonov
dcricket5@vimeo.com	89755523	RiGBgCul	Delmore Cricket
kgoter6@nytimes.com	838555726	5ET3I3yv	Katrina Goter
tfernez7@thetimes.co.uk	855555151	l3dJKQvw0Vz	Tobie Fernez
lhusband8@chronoengine.com	856555739	fRqwlu3	Lovell Husband
hboyall9@ebay.co.uk	838555918	IASmWxJQD23	Herman Boyall
wgammadea@jimdo.com	897555283	IwUO9832RFs2	Waverley Gammade
clethleyb@ox.ac.uk	878555238	DIyMz94Kp9OY	Carmelia Lethley
whanbidgec@bloglovin.com	838555451	Z86XSIfi	Wendi Hanbidge
rtrimmilld@blogger.com	838555851	sMeSfN	Robinia Trimmill
pestabrooke@lulu.com	853555198	mqsh8Nwic	Pier Estabrook
rjoyesf@tuttocitta.it	853555588	egxwbvpe8W	Roley Joyes
odrengg@va.gov	815555562	5K4ZPa5	Odelle Dreng
ndrewsonh@uol.com.br	899555926	JcczpT1C1e	Nancey Drewson
dbeeswingi@thetimes.co.uk	897555105	0jMpiKJzZou	Doe Beeswing
ekibardj@twitpic.com	838555696	7fqcwQ8pSckj	Eleanore Kibard
cwetheyk@g.co	878555402	SDijjSYJV3P	Caren Wethey
tmacpheel@webeden.co.uk	878555512	NlCUa1	Torre MacPhee
avallerinem@reference.com	854555487	cdI0E8Lm	Ariel Vallerine
bpoldingn@printfriendly.com	838555962	jBmdFXvwDQn	Blondie Polding
qrikkardo@phpbb.com	838555982	TFX8hM7SDfs	Quintin Rikkard
\.


--
-- Data for Name: produk; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.produk (id_produk) FROM stdin;
pdd01
pdd02
pdd03
pdd04
pdd05
pdd06
pdd07
pdd08
pdd09
pdd10
pdd11
pdd12
pdd13
pdd14
pdd15
pdd16
pdd17
pdd18
pdd19
pdd20
\.


--
-- Data for Name: produk_apotek; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.produk_apotek (harga_jual, stok, satuan_penjualan, id_produk, id_apotek) FROM stdin;
55000	30	strip	pdd11	apd04
90500	70	strip	pdd08	apd01
29300	50	botol	pdd19	apd02
134000	34	botol	pdd13	apd03
15000	79	strip	pdd05	apd05
\.


--
-- Data for Name: transaksi_dengan_resep; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.transaksi_dengan_resep (no_resep, nama_dokter, nama_pasien, isi_resep, unggahan_resep, tanggal_resep, status_validasi, no_telepon, id_transaksi_pembelian, email_apoteker) FROM stdin;
6167981493	Juglans regia L.	Lamprotornis nitens	velit id pretium iaculis	dis parturient monte	2020-02-27	tersedia	7305532223	trd06	odrengg@va.gov
1759225118	Hexastylis virginica 	Odocoilenaus virginianus	penatibus et magn	blandit nam nulla integer 	2019-08-30	kosong	7985542800	trd07	tmacpheel@webeden.co.uk
2471006213	Polygonum convolvulus L. 	Varanus sp.	euismod sceleri	hac ha	2020-01-22	tersedia	4161277225	trd08	ntotaro2@webs.com
2124342223	Hesperostipa comata 	Spizaetus coronatus	nisi volutpat eleifen	justo in hac	2020-01-14	kosong	9417165056	trd09	lhusband8@chronoengine.com
5226843585	Heterotheca sessiliflora 	Castor canadensis	ante vel ipsum praesent blandit	justo  ut suscipit a 	2019-10-20	kosong	8281556648	trd10	bpoldingn@printfriendly.com
\.


--
-- Data for Name: transaksi_online; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.transaksi_online (no_urut_pembelian, id_transaksi_pembelian) FROM stdin;
nmr014	trd04
nmr020	trd05
nmr004	trd06
nmr009	trd07
nmr006	trd08
\.


--
-- Data for Name: transaksi_pembelian; Type: TABLE DATA; Schema: farmakami; Owner: dbi34
--

COPY farmakami.transaksi_pembelian (id_transaksi_pembelian, waktu_pembelian, total_pembayaran, id_konsumen) FROM stdin;
trd02	2019-12-08 23:50:02	391000	kd02
trd03	2019-06-20 12:29:37	154000	kd03
trd04	2019-11-21 19:15:47	308000	kd04
trd05	2018-06-27 08:22:26	440000	kd05
trd06	2018-12-24 19:08:01	171000	kd01
trd07	2019-11-02 10:57:38	454000	kd02
trd08	2018-11-14 10:31:06	421000	kd03
trd09	2019-05-08 00:17:32	160000	kd04
trd10	2020-02-25 04:38:02	223000	kd01
trd11	2020-03-01 16:48:56	267000	kd02
trd12	2020-02-19 04:00:51	236000	kd03
trd13	2020-03-04 11:26:28	283000	kd01
trd14	2020-01-26 13:22:03	196000	kd02
trd15	2020-02-19 13:02:38	333000	kd03
trd01	2018-11-14 04:00:24	284000	kd01
\.


--
-- Name: admin_apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.admin_apotek
    ADD CONSTRAINT admin_apotek_pkey PRIMARY KEY (email);


--
-- Name: alamat_konsumen_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.alamat_konsumen
    ADD CONSTRAINT alamat_konsumen_pkey PRIMARY KEY (id_konsumen, alamat, status);


--
-- Name: alat_medis_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.alat_medis
    ADD CONSTRAINT alat_medis_pkey PRIMARY KEY (id_alat_medis);


--
-- Name: apotek_alamat_apotek_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_alamat_apotek_key UNIQUE (alamat_apotek);


--
-- Name: apotek_email_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_email_key UNIQUE (email);


--
-- Name: apotek_no_sia_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_no_sia_key UNIQUE (no_sia);


--
-- Name: apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.apotek
    ADD CONSTRAINT apotek_pkey PRIMARY KEY (id_apotek);


--
-- Name: apoteker_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.apoteker
    ADD CONSTRAINT apoteker_pkey PRIMARY KEY (email);


--
-- Name: balai_apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.balai_apotek
    ADD CONSTRAINT balai_apotek_pkey PRIMARY KEY (id_balai, id_apotek);


--
-- Name: balai_pengobatan_alamat_balai_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.balai_pengobatan
    ADD CONSTRAINT balai_pengobatan_alamat_balai_key UNIQUE (alamat_balai);


--
-- Name: balai_pengobatan_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.balai_pengobatan
    ADD CONSTRAINT balai_pengobatan_pkey PRIMARY KEY (id_balai);


--
-- Name: cs_no_sia_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.cs
    ADD CONSTRAINT cs_no_sia_key UNIQUE (no_sia);


--
-- Name: cs_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.cs
    ADD CONSTRAINT cs_pkey PRIMARY KEY (no_ktp);


--
-- Name: kandungan_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.kandungan
    ADD CONSTRAINT kandungan_pkey PRIMARY KEY (id_bahan, id_obat, takaran);


--
-- Name: kategori_merk_obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.kategori_merk_obat
    ADD CONSTRAINT kategori_merk_obat_pkey PRIMARY KEY (id_kategori, id_merk_obat);


--
-- Name: kategori_obat_nama_kategori_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.kategori_obat
    ADD CONSTRAINT kategori_obat_nama_kategori_key UNIQUE (nama_kategori);


--
-- Name: kategori_obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.kategori_obat
    ADD CONSTRAINT kategori_obat_pkey PRIMARY KEY (id_kategori);


--
-- Name: komposisi_nama_bahan_key; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.komposisi
    ADD CONSTRAINT komposisi_nama_bahan_key UNIQUE (nama_bahan);


--
-- Name: komposisi_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.komposisi
    ADD CONSTRAINT komposisi_pkey PRIMARY KEY (id_bahan);


--
-- Name: konsumen_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.konsumen
    ADD CONSTRAINT konsumen_pkey PRIMARY KEY (id_konsumen);


--
-- Name: kurir_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.kurir
    ADD CONSTRAINT kurir_pkey PRIMARY KEY (id_kurir);


--
-- Name: list_produk_dibeli_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.list_produk_dibeli
    ADD CONSTRAINT list_produk_dibeli_pkey PRIMARY KEY (id_apotek, id_produk, id_transaksi_pembelian);


--
-- Name: merk_dagang_alat_medis_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.merk_dagang_alat_medis
    ADD CONSTRAINT merk_dagang_alat_medis_pkey PRIMARY KEY (id_alat_medis, id_merk, fitur);


--
-- Name: merk_dagang_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.merk_dagang
    ADD CONSTRAINT merk_dagang_pkey PRIMARY KEY (id_merk);


--
-- Name: merk_obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.merk_obat
    ADD CONSTRAINT merk_obat_pkey PRIMARY KEY (id_merk_obat);


--
-- Name: obat_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.obat
    ADD CONSTRAINT obat_pkey PRIMARY KEY (id_obat);


--
-- Name: pengantaran_farmasi_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.pengantaran_farmasi
    ADD CONSTRAINT pengantaran_farmasi_pkey PRIMARY KEY (id_pengantaran);


--
-- Name: pengguna_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (email);


--
-- Name: produk_apotek_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.produk_apotek
    ADD CONSTRAINT produk_apotek_pkey PRIMARY KEY (id_produk, id_apotek);


--
-- Name: produk_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.produk
    ADD CONSTRAINT produk_pkey PRIMARY KEY (id_produk);


--
-- Name: transaksi_dengan_resep_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.transaksi_dengan_resep
    ADD CONSTRAINT transaksi_dengan_resep_pkey PRIMARY KEY (no_resep);


--
-- Name: transaksi_online_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.transaksi_online
    ADD CONSTRAINT transaksi_online_pkey PRIMARY KEY (no_urut_pembelian);


--
-- Name: transaksi_pembelian_pkey; Type: CONSTRAINT; Schema: farmakami; Owner: dbi34; Tablespace: 
--

ALTER TABLE ONLY farmakami.transaksi_pembelian
    ADD CONSTRAINT transaksi_pembelian_pkey PRIMARY KEY (id_transaksi_pembelian);


--
-- Name: trigger_ubah_kurir; Type: TRIGGER; Schema: farmakami; Owner: dbi34
--

CREATE TRIGGER trigger_ubah_kurir BEFORE UPDATE OF id_kurir ON farmakami.pengantaran_farmasi FOR EACH ROW EXECUTE PROCEDURE farmakami.ubah_kurir();


--
-- Name: trigger_update_total_biaya_pengantaran; Type: TRIGGER; Schema: farmakami; Owner: dbi34
--

CREATE TRIGGER trigger_update_total_biaya_pengantaran AFTER INSERT ON farmakami.pengantaran_farmasi FOR EACH ROW EXECUTE PROCEDURE farmakami.update_total_biaya_pengantaran();


--
-- Name: admin_apotek_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.admin_apotek
    ADD CONSTRAINT admin_apotek_email_fkey FOREIGN KEY (email) REFERENCES farmakami.apoteker(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: admin_apotek_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.admin_apotek
    ADD CONSTRAINT admin_apotek_id_apotek_fkey FOREIGN KEY (id_apotek) REFERENCES farmakami.apotek(id_apotek) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alamat_konsumen_id_konsumen_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.alamat_konsumen
    ADD CONSTRAINT alamat_konsumen_id_konsumen_fkey FOREIGN KEY (id_konsumen) REFERENCES farmakami.konsumen(id_konsumen) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: alat_medis_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.alat_medis
    ADD CONSTRAINT alat_medis_id_produk_fkey FOREIGN KEY (id_produk) REFERENCES farmakami.produk(id_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: apoteker_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.apoteker
    ADD CONSTRAINT apoteker_email_fkey FOREIGN KEY (email) REFERENCES farmakami.pengguna(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: balai_apotek_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.balai_apotek
    ADD CONSTRAINT balai_apotek_id_apotek_fkey FOREIGN KEY (id_apotek) REFERENCES farmakami.apotek(id_apotek) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: balai_apotek_id_balai_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.balai_apotek
    ADD CONSTRAINT balai_apotek_id_balai_fkey FOREIGN KEY (id_balai) REFERENCES farmakami.balai_pengobatan(id_balai) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cs_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.cs
    ADD CONSTRAINT cs_email_fkey FOREIGN KEY (email) REFERENCES farmakami.apoteker(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kandungan_id_bahan_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.kandungan
    ADD CONSTRAINT kandungan_id_bahan_fkey FOREIGN KEY (id_bahan) REFERENCES farmakami.komposisi(id_bahan) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kandungan_id_obat_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.kandungan
    ADD CONSTRAINT kandungan_id_obat_fkey FOREIGN KEY (id_obat) REFERENCES farmakami.obat(id_obat) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_merk_obat_id_kategori_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.kategori_merk_obat
    ADD CONSTRAINT kategori_merk_obat_id_kategori_fkey FOREIGN KEY (id_kategori) REFERENCES farmakami.kategori_obat(id_kategori) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kategori_merk_obat_id_merk_obat_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.kategori_merk_obat
    ADD CONSTRAINT kategori_merk_obat_id_merk_obat_fkey FOREIGN KEY (id_merk_obat) REFERENCES farmakami.merk_obat(id_merk_obat) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: konsumen_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.konsumen
    ADD CONSTRAINT konsumen_email_fkey FOREIGN KEY (email) REFERENCES farmakami.pengguna(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kurir_email_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.kurir
    ADD CONSTRAINT kurir_email_fkey FOREIGN KEY (email) REFERENCES farmakami.pengguna(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: list_produk_dibeli_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.list_produk_dibeli
    ADD CONSTRAINT list_produk_dibeli_id_produk_fkey FOREIGN KEY (id_produk, id_apotek) REFERENCES farmakami.produk_apotek(id_produk, id_apotek);


--
-- Name: list_produk_dibeli_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.list_produk_dibeli
    ADD CONSTRAINT list_produk_dibeli_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian);


--
-- Name: merk_dagang_alat_medis_id_alat_medis_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.merk_dagang_alat_medis
    ADD CONSTRAINT merk_dagang_alat_medis_id_alat_medis_fkey FOREIGN KEY (id_alat_medis) REFERENCES farmakami.alat_medis(id_alat_medis) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: merk_dagang_alat_medis_id_merk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.merk_dagang_alat_medis
    ADD CONSTRAINT merk_dagang_alat_medis_id_merk_fkey FOREIGN KEY (id_merk) REFERENCES farmakami.merk_dagang(id_merk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: obat_id_merk_obat_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.obat
    ADD CONSTRAINT obat_id_merk_obat_fkey FOREIGN KEY (id_merk_obat) REFERENCES farmakami.merk_obat(id_merk_obat) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: obat_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.obat
    ADD CONSTRAINT obat_id_produk_fkey FOREIGN KEY (id_produk) REFERENCES farmakami.produk(id_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengantaran_farmasi_id_kurir_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.pengantaran_farmasi
    ADD CONSTRAINT pengantaran_farmasi_id_kurir_fkey FOREIGN KEY (id_kurir) REFERENCES farmakami.kurir(id_kurir) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: pengantaran_farmasi_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.pengantaran_farmasi
    ADD CONSTRAINT pengantaran_farmasi_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: produk_apotek_id_apotek_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.produk_apotek
    ADD CONSTRAINT produk_apotek_id_apotek_fkey FOREIGN KEY (id_apotek) REFERENCES farmakami.apotek(id_apotek) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: produk_apotek_id_produk_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.produk_apotek
    ADD CONSTRAINT produk_apotek_id_produk_fkey FOREIGN KEY (id_produk) REFERENCES farmakami.produk(id_produk) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_dengan_resep_email_apoteker_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.transaksi_dengan_resep
    ADD CONSTRAINT transaksi_dengan_resep_email_apoteker_fkey FOREIGN KEY (email_apoteker) REFERENCES farmakami.apoteker(email) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_online_id_transaksi_pembelian_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.transaksi_online
    ADD CONSTRAINT transaksi_online_id_transaksi_pembelian_fkey FOREIGN KEY (id_transaksi_pembelian) REFERENCES farmakami.transaksi_pembelian(id_transaksi_pembelian) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: transaksi_pembelian_id_konsumen_fkey; Type: FK CONSTRAINT; Schema: farmakami; Owner: dbi34
--

ALTER TABLE ONLY farmakami.transaksi_pembelian
    ADD CONSTRAINT transaksi_pembelian_id_konsumen_fkey FOREIGN KEY (id_konsumen) REFERENCES farmakami.konsumen(id_konsumen) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

