from django.urls import path
from . import views

app_name = "pengantaranfarmasi"

urlpatterns = [
    path('create-pengantaranfarmasi/', views.create_pf_view, name='create-pf'),
    path('daftar-pengantaranfarmasi/',views.daftar_pf_view,name='daftar-pf'),
    path('update-pengantaranfarmasi/',views.update_pf_view,name='update-pf'),
  
]
