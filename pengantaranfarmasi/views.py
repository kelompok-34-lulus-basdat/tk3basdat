from django.shortcuts import render

# Create your views here.
def create_pf_view(request):
    return render(request, 'create-pf.html')
def daftar_pf_view(request):
    return render(request,'daftar-pf.html')
def update_pf_view(request):
    return render(request,'update-pf.html')
