from django.urls import path
from . import views

app_name = "listprodukdibeli"

urlpatterns = [
    path('create-listprodukdibeli/', views.create_listpd_view, name='create-listpd'),
    path('daftar-listprodukdibeli/',views.daftar_listpd_view,name='daftar-listpd'),
    path('update-listprodukdibeli/',views.update_listpd_view,name='update-listpd'),
  
]
