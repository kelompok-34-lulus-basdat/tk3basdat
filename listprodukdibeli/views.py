from django.shortcuts import render

# Create your views here.
def create_listpd_view(request):
    return render(request, 'create-listpd.html')
def daftar_listpd_view(request):
    return render(request,'daftar-listpd.html')
def update_listpd_view(request):
    return render(request,'update-listpd.html')