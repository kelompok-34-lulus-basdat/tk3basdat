from django.shortcuts import render, redirect
from django.db import connection

# Create your views here.
def create_apotek(request):
    apotek = {
        "id-apotek" : get_id_apotek(),
        "nama-apotek" : request.POST.get('nama-apotek'),
        "alamat-apotek" : request.POST.get("alamat-apotek"),
        "telepon-apotek" : request.POST.get("telepon-apotek"),
        "nama_penyelenggara" : request.POST.get("nama-penyelenggara"),
        "no-sia" : request.POST.get("no-sia"),
        "email-" : request.POST.get("email-")
    }

    query = """
    INSERT INTO APOTEK (id_apotek, email, no_sia, nama_penyelenggara, nama_apotek, alamat_apotek, telepon_apotek)
    VALUES('%s', '%s', '%s', '%s', '%s', '%s', '%s');
    """ %(apotek['id-apotek'], apotek['email-'], apotek['no-sia'], apotek['nama-penyelenggara'], apotek['nama-apotek'], apotek['alamat-apotek'], apotek['telepon-apotek'])

    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('apotek:mylistapotek')

def form_create_apotek(request):
    return render(request, 'create-apotek.html')

def update_apotek(request, id):
    apotek = {
        "id-apotek" : request.POST.get('u-id-apotek'),
        "nama-apotek" : request.POST.get('u-nama-apotek'),
        "alamat-apotek" : request.POST.get("u-alamat-apotek"),
        "telepon-apotek" : request.POST.get("u-telepon-apotek"),
        "nama-penyelenggara" : request.POST.get("u-nama-penyelenggara"),
        "no-sia" : request.POST.get("u-no-sia"),
        "email-" : request.POST.get("u-email")
    }
    print (apotek)
    query = """
    UPDATE APOTEK
    SET email='%s', no_sia='%s', nama_penyelenggara='%s', nama_apotek='%s', alamat_apotek='%s', telepon_apotek='%s'
    WHERE id_apotek = '%s';
    """ %(apotek['email-'], apotek['no-sia'], apotek['nama-penyelenggara'], apotek['nama-apotek'], apotek['alamat-apotek'], apotek['telepon-apotek'], id)
    
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('apotek:mylistapotek')

def list_apotek(request):
    query = """
    SELECT * 
    FROM APOTEK
    ORDER BY CAST(id_apotek AS varchar);
    """
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    data_apotek = fetch(cursor)

    return render(request, 'list-apotek.html', {'data_apotek':data_apotek})

def delete_apotek(request, id):
    query ="""
    DELETE FROM APOTEK 
    WHERE id_apotek = '%s';
    """ %(id)
    
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return redirect('apotek:mylistapotek')

# converts sql rows to a dict
def fetch(cursor):
	columns = [col[0] for col in cursor.description]
	return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_id_apotek():
    query = """
    SELECT CONCAT('apd', CAST (last as integer)+1)
    FROM ( SELECT SUBSTRING(id_apotek FROM 4 ) as last FROM APOTEK
    ORDER BY id_apotek DESC LIMIT 1) A;
	"""
    cursor = connection.cursor()
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    id_apotek_baru = str(cursor.fetchone()[0])

    query = """
    INSERT INTO APOTEK(id_apotek)
    VALUES ('%s')
    """%(str(id_apotek_baru))
    cursor.execute("SET search_path TO FARMAKAMI;")
    cursor.execute(query)

    return id_apotek_baru

def get_as_dict(cursor):
    columns = [column[0] for column in cursor.description]
    results = []
    for row in cursor.fetchall():
        results.append(dict(zip(columns, row)))
    return results