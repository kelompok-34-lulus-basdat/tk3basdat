from django.urls import path
from . import views

app_name = "apotek"

urlpatterns = [
    path('createapotek/', views.create_apotek, name='mycreateapotek'),
    path('formcreateapotek/', views.form_create_apotek, name='myformcreateapotek'),
    path('updateapotek/<id>', views.update_apotek, name='myupdateapotek'),
    path('listapotek/', views.list_apotek, name='mylistapotek'),
    path('deleteapotek/<id>', views.delete_apotek, name='mydeleteapotek<id>'),
]
